# dotfiles

Arquivos de configuração do meu desktop minimalista:

## Distribuição: Debian testing

- **I3-WM**

- I3status

- Notificações: Dunst

- Emulador de terminal: RXVT Unicode

- BASH

- Gerenciador de arquivos: Ranger

- Navegador QuteBrowser

- vim

